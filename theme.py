from enum import Enum


class Theme(str, Enum):
    blue = "#4292c3"
    dark_grey = "#2e2e2e"
    light_grey = "#494d58"
    pink = "#de4e93"
    white = "#e3f2ff"
    transparent = "#00000000"
