from typing import Callable

from libqtile import hook, layout
from libqtile.config import Group, Key
from libqtile.layout.base import Layout
from libqtile.lazy import lazy

from theme import Theme


def _column_keys(mod: str) -> tuple[Key, ...]:
    return (
        Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
        Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
        Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
        Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
        # Move windows between left/right columns or move up/down in current stack.
        # Moving out of range in Columns layout will create new column.
        Key(
            [mod, "shift"],
            "h",
            lazy.layout.shuffle_left(),
            desc="Move window to the left",
        ),
        Key(
            [mod, "shift"],
            "l",
            lazy.layout.shuffle_right(),
            desc="Move window to the right",
        ),
        Key(
            [mod, "shift"],
            "j",
            lazy.layout.shuffle_down(),
            desc="Move window down",
        ),
        Key(
            [mod, "shift"],
            "k",
            lazy.layout.shuffle_up(),
            desc="Move window up",
        ),
        # Grow windows. If current window is on the edge of screen and direction
        # will be to screen edge - window would shrink.
        Key(
            [mod, "control"],
            "h",
            lazy.layout.grow_left(),
            desc="Grow window to the left",
        ),
        Key(
            [mod, "control"],
            "l",
            lazy.layout.grow_right(),
            desc="Grow window to the right",
        ),
        Key(
            [mod, "control"],
            "j",
            lazy.layout.grow_down(),
            desc="Grow window down",
        ),
        Key(
            [mod, "control"],
            "k",
            lazy.layout.grow_up(),
            desc="Grow window up",
        ),
        Key(
            [mod, "control"],
            "n",
            lazy.layout.normalize(),
            desc="Reset all window sizes",
        ),
        # Toggle between split and unsplit sides of stack.
        # Split = all windows displayed
        # Unsplit = 1 window displayed, like Max layout, but still with
        # multiple stack panes
        Key(
            [mod, "shift"],
            "Return",
            lazy.layout.toggle_split(),
            desc="Toggle between split and unsplit sides of stack",
        ),
    )


def on_layout_changed(
    mod: str, master_keys: tuple[Key, ...], keys: list[Key]
) -> Callable[[Layout, Group], None]:
    def _inner(_layout: Layout, group: Group) -> None:
        if isinstance(_layout, layout.Columns):
            keys.clear()
            keys.extend(master_keys)
            keys.extend(_column_keys(mod))

    return _inner


def setup_layouts(mod: str, master_keys: tuple[Key, ...], keys: list[Key]):
    layouts = [
        layout.Columns(
            border_focus=Theme.light_grey,
            border_normal=Theme.dark_grey,
            border_focus_stack=Theme.blue,
            border_width=1,
            # margin=[0, 5, 0, 5],
            margin=4,
            margin_on_single=[4, 40, 4, 40],
            num_columns=2,
        ),
        # layout.Floating(),
        layout.Max(),
        # layout.Stack(num_stacks=2),
        # layout.Bsp(),
        # layout.Matrix(),
        # layout.MonadTall(),
        # layout.MonadWide(),
        # layout.RatioTile(),
        # layout.Tile(),
        # layout.TreeTab(),
        # layout.VerticalTile(),
        # layout.Zoomy(),
    ]

    # setup hooks
    hook.subscribe.layout_change(on_layout_changed(mod, master_keys, keys))

    return layouts
