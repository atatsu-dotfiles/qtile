from libqtile.config import DropDown, Group, Key, ScratchPad
from libqtile.lazy import lazy


def setup_groups(mod: str, keys: list[Key]) -> list[Group | ScratchPad]:
    """
    Setup groups. Mutates the list of `keys` in place.
    """
    groups: list[Group | ScratchPad] = [Group(i, label="⏺") for i in "123"]
    for i in groups:
        keys.extend(
            [
                # mod1 + letter of group = switch to group
                Key(
                    [mod],
                    i.name,
                    lazy.group[i.name].toscreen(),
                    desc="Switch to group {}".format(i.name),
                ),
                # mod1 + shift + letter of group = switch to & move focused window to group
                Key(
                    [mod, "shift"],
                    i.name,
                    lazy.window.togroup(i.name, switch_group=True),
                    desc="Switch to & move focused window to group {}".format(i.name),
                ),
                # Or, use below if you prefer not to switch to that group.
                # # mod1 + shift + letter of group = move focused window to group
                # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
                #     desc="move focused window to group {}".format(i.name)),
            ]
        )
    groups.extend(
        [
            ScratchPad(
                "scratchpad",
                [
                    DropDown("term", "alacritty", opacity=0.8),
                    DropDown(
                        "qtile shell",
                        "alacritty --hold -e 'qtile shell'",
                        on_focus_lost_hide=True,
                    ),
                ],
            )
        ]
    )
    keys.extend(
        [
            Key([mod], "grave", lazy.group["scratchpad"].dropdown_toggle("term")),
            Key(
                [mod, "shift"],
                "grave",
                lazy.group["scratchpad"].dropdown_toggle("qtile shell"),
            ),
        ]
    )
    return groups
