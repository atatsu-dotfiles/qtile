from libqtile import layout
from libqtile.config import Match
from libqtile.utils import guess_terminal

from keys import setup_global_keys
from groups import setup_groups
from layouts import setup_layouts
from mouse import setup_mouse
from screens import setup_screens
from theme import Theme


mod = "mod4"
terminal = guess_terminal()

keys = setup_global_keys(mod, terminal)
groups = setup_groups(mod, keys)
master_keys = tuple(keys)
layouts = setup_layouts(mod, master_keys, keys)
screens = setup_screens()
mouse = setup_mouse(mod)

# If a window requests to be fullscreen, it is automatically fullscreened.
auto_fullscreen = True
# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True
# When clicked, should the window be brought to the front or not. If this is set
# to "floating_only", only floating windows will get affected (This sets the X
# Stack Mode to Above.). This will ignore the layering rules and will therefore
# bring windows above other windows, even if they have been set as "kept_above".
# This may cause issues with docks and other similar apps as these may end up
# hidden behind other windows. Setting this to False or "floating_only" may
# therefore be required when using these apps.
bring_front_click = False
# If true, the cursor follows the focus as directed by the keyboard, warping to
# the center of the focused window. When switching focus between screens, If
# there are no windows in the screen, the cursor will warp to the center of the screen.
cursor_warp = True
# A list of Rule objects which can send windows to various groups based on
# matching criteria.
dgroups_app_rules = []  # type: list
# A function which generates group binding hotkeys. It takes a single argument,
# the DGroups object, and can use that to set up dynamic key bindings.
# A sample implementation is available in libqtile/dgroups.py called
# simple_key_binder(), which will bind groups to mod+shift+0-10 by default.
dgroups_key_binder = None
# The default floating layout to use. This allows you to set custom floating
# rules among other things if you wish.
# See the configuration file for the default float_rules.
floating_layout = layout.Floating(
    border_focus=Theme.light_grey,
    border_normal=Theme.dark_grey,
    float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        *layout.Floating.default_float_rules,
        Match(wm_class="confirmreset"),  # gitk
        Match(wm_class="makebranch"),  # gitk
        Match(wm_class="maketag"),  # gitk
        Match(wm_class="ssh-askpass"),  # ssh-askpass
        Match(title="branchdialog"),  # gitk
        Match(title="pinentry"),  # GPG key password entry
    ],
)
# Floating windows are kept above tiled windows (Currently x11 only. Wayland
# support coming soon.)
floats_kept_above = True
# self explanatory
follow_mouse_focus = True
# Behavior of the _NET_ACTIVATE_WINDOW message sent by applications
#  * urgent: urgent flag is set for the window
#  * focus: automatically focus the window
#  * smart: automatically focus if the window is in the current group
#  * never: never automatically focus any window that requests it
focus_on_window_activation = "smart"
# Controls whether or not to automatically reconfigure screens when
# there are changes in randr output configuration.
reconfigure_screens = True
# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None
# Default settings for bar widgets.
widget_defaults = dict(
    font="sans",
    fontsize=12,
    padding=3,
)
# Default settings for extensions.
extension_defaults = widget_defaults.copy()
# LIe about wm name so certain java apps work
wmname = "LG3D"
