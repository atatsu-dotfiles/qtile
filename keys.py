from libqtile.config import Key
from libqtile.lazy import lazy


@lazy.function
def print_kb(qtile):
    print(qtile, qtile.display_kb())


def setup_global_keys(mod, terminal) -> list[Key]:
    """
    List of global keys that affects every screen/layout.
    """
    return [
        Key([mod], "s", print_kb(), desc="Display table of key bindings"),
        Key([mod], "n", lazy.screen.next_group(), desc="Move to the next Group"),
        Key([mod], "p", lazy.screen.prev_group(), desc="Move to the previous Group"),
        Key(
            [mod], "space", lazy.layout.next(), desc="Move window focus to other window"
        ),
        Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
        # Toggle between different layouts as defined below
        Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
        Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),
        Key(
            [mod],
            "f",
            lazy.window.toggle_fullscreen(),
            desc="Toggle fullscreen on the focused window",
        ),
        Key(
            [mod],
            "t",
            lazy.window.toggle_floating(),
            desc="Toggle floating on the focused window",
        ),
        Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
        Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
        Key(
            [mod],
            "d",
            lazy.spawn(
                'rofi -show combi -modes combi -combi-modes "window,drun,run" -theme arthur.rasi'
            ),
            desc="Application launcher",
        ),
    ]


def setup_columns_keys(mod):
    """
    Key bindings specific to the `libqtile.layout.Columns` layout.
    """
    return []
