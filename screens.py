from libqtile import bar, widget
from libqtile.config import Screen

from theme import Theme


def setup_screens() -> list[Screen]:
    screens = [
        Screen(
            bottom=bar.Bar(
                [
                    widget.CurrentLayoutIcon(),
                    widget.CurrentLayout(),
                    widget.GroupBox(
                        active=Theme.blue,
                        block_highlight_text_color=Theme.pink,
                        inactive=Theme.light_grey,
                        borderwidth=0,
                    ),
                    widget.WindowCount(),
                    widget.WindowName(),
                    widget.TaskList(),
                    widget.Chord(
                        chords_colors={
                            "launch": ("#ff0000", "#ffffff"),
                        },
                        name_transform=lambda name: name.upper(),
                    ),
                    widget.PulseVolume(emoji=True),
                    # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                    # widget.StatusNotifier(),
                    widget.Systray(),
                    widget.Clock(format="%Y-%m-%d %a %I:%M %p"),
                    widget.QuickExit(default_text="󰍃", countdown_format="[{}]"),
                ],
                24,
                background=Theme.dark_grey,
                # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
                # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
            ),
            # You can uncomment this variable if you see that on X11 floating resize/moving is laggy
            # By default we handle these events delayed to already improve performance, however your system might still be struggling
            # This variable is set to None (no cap) by default, but you can set it to 60 to indicate that you limit it to 60 events per second
            # x11_drag_polling_rate = 60,
        ),
    ]
    return screens
